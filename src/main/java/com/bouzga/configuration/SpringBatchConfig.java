package com.bouzga.configuration;

import com.bouzga.entities.BankTransaction;
import com.bouzga.listners.JobListener;
import com.bouzga.listners.ReadListener;
import com.bouzga.listners.StepListener;
import com.bouzga.listners.WriteListener;
import com.bouzga.processor.BankTransactionProcessor;
import com.bouzga.reader.BankTransactionFlatFileReader;
import com.bouzga.writer.BankTransactionWriter;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private BankTransactionFlatFileReader bankTransactionItemReader;

    @Autowired
    private BankTransactionWriter bankTransactionItemWriter;

    @Autowired
    private BankTransactionProcessor bankTransactionItemProcessor;

    @Value("${inputFile}")
    private Resource bankTransactionDataResource;


    @Bean
    public ItemReadListener<BankTransaction> readListener() {
        return new ReadListener<>();
    }

    @Bean
    public StepExecutionListener stepListener() {
        return new StepListener();
    }

    @Bean
    public ItemWriteListener<BankTransaction> writeListener() {
        return new WriteListener<>();
    }

    @Bean
    public JobExecutionListener jobListener() {
        return new JobListener();
    }

    @Bean
    public Step step1(){
        Step step = stepBuilderFactory.get("load-bank-step-1")
                .listener(stepListener())
                .listener(readListener())
                .listener(writeListener())
                .<BankTransaction,BankTransaction>chunk(100)
                .reader(getItemBankTransactionItemReader())
                .writer(bankTransactionItemWriter)
                .processor(bankTransactionItemProcessor)
                .build();
        return step;
    }

    @Bean
    public Job job1(){

        return jobBuilderFactory.get("load-bank-job-1")
                .listener(jobListener())
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    @Bean
    public Job job2(){

        Step step = stepBuilderFactory.get("load-bank-step")
                .<BankTransaction,BankTransaction>chunk(1000)
                .reader(getItemBankTransactionItemReader())
                .writer(bankTransactionItemWriter)
                .processor(bankTransactionItemProcessor)
                .build();

        return jobBuilderFactory.get("load-bank-job-2")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    @Bean
    public FlatFileItemReader<BankTransaction> getItemBankTransactionItemReader(){

        FlatFileItemReader<BankTransaction> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setName("CSV-READER");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setResource(bankTransactionDataResource);
        flatFileItemReader.setLineMapper(lineMapper());
        return flatFileItemReader;
    }

    @Bean
    public static LineMapper<BankTransaction> lineMapper() {
        DefaultLineMapper<BankTransaction> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("id","account_number","strTransactionDate","type","amount");
        lineMapper.setLineTokenizer(lineTokenizer);
        BeanWrapperFieldSetMapper<BankTransaction> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(BankTransaction.class);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        return lineMapper;
    }


    @Bean
    public BankTransactionFlatFileReader getBankTransactionFlatFileReader(){
        return new BankTransactionFlatFileReader();
    }

    @Bean
    public BankTransactionProcessor getBankTransactionItemProcessor(){
        return new BankTransactionProcessor();
    }

    @Bean
    public BankTransactionWriter getBankTransactionItemWriter(){
        return new BankTransactionWriter();
    }

}
