package com.bouzga;

import lombok.Data;

import java.io.*;
import java.util.concurrent.ThreadLocalRandom;

@Data
public class CreateData {

    public static void main(String[] args) throws IOException {
        writeFile();
    }

    public static void writeFile() throws IOException {
        File fout = new File("/Users/bouzga/REPO_AUTO_FORMATION/spring-batch-bank/src/main/resources/data.csv");
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write("id,account_number,date,type,amount");
        bw.newLine();
        for (int i = 1; i <= 2000; i++) {
            int account_number = ThreadLocalRandom.current().nextInt(1, 1000000);

            int dayNumber = ThreadLocalRandom.current().nextInt(10, 28);
            int mounthNumer = ThreadLocalRandom.current().nextInt(10, 12);
            int yearNumber = ThreadLocalRandom.current().nextInt(2011, 2022);
            int hourNumber = ThreadLocalRandom.current().nextInt(10, 23);
            int minuteNumber = ThreadLocalRandom.current().nextInt(10, 59);
            double amount = ThreadLocalRandom.current().nextDouble(1, 99999);
            String date = dayNumber+"/"+mounthNumer+"/"+yearNumber+"-"+hourNumber+":"+minuteNumber;
            String type = Math.random()>0.5?"C":"D";
            StringBuilder str = new StringBuilder(i+"")
                    .append(",")
                    .append(account_number)
                    .append(",")
                    .append(date)
                    .append(",")
                    .append(type)
                    .append(",")
                    .append(amount);
            bw.write(str.toString());
            bw.newLine();
        }

        bw.close();
    }
}
